<?php

/*
* @Author 	: Dicky Ermawan S., S.T., MTA
* @Email 	: wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date 	: 2018-05-04 23:27:29
* @Last Modified by	 : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-05-08 00:35:51
*/

namespace app\controllers;

use Yii;
use app\models\Pengguna;
use app\models\Rujukan;
use app\models\RujukanSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use ElephantIO\Client as Socket;
use ElephantIO\Engine\SocketIO\Version1X;

/**
 * RujukanController implements the CRUD actions for Rujukan model.
 */
class RujukanController extends Controller
{
    protected $socket;
    protected $pageSize;

    /**
     * Inisialisasi awal sebelum method dieksekusi.
     * @return void
     */
    public function init()
    {
        $this->socket = new Socket(new Version1X('127.0.0.1:3000'));
        $this->pageSize = 15;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Rujukan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RujukanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, Yii::$app->user->identity->id);

        // print_r($searchModel); exit;
        // print_r($dataProvider); exit;

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Rujukan model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Rujukan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Rujukan();

        if ($model->load(Yii::$app->request->post())) {
            date_default_timezone_set("Asia/Jakarta");
            $model->tgl_masuk = date('Y-m-d  H:i:s');
            $model->ibu_tglahir = date('Y-m-d',$model->ibu_tglahir);

            if($model->bayi_tglahir!=null)
                $model->bayi_tglahir = date('Y-m-d',$model->bayi_tglahir);

            $model->status = Rujukan::MENUNGGU;

            if($model->save()){
                Yii::$app->getSession()->setFlash('success', 'Data Rujukan berhasil dikirimkan, harap menunggu respon.');

                //untuk socket
                $this->socket->initialize();
                $this->socket->emit('create_guestbook', []);
                $this->socket->close();

                //NANTI DIAKTIFKAN LAGI, ini utk ngetes biar form terisi terus
                // return $this->redirect(['rujukan/index']);
            }
            // else{
            //     print_r($model);
            // }
        }

        $model->asal_rujukan = Yii::$app->user->identity->id;
        $model->tujuan_rujukan = 2;

        $model->tujuan_r = Pengguna::findOne($model->tujuan_rujukan)->nama_rs_puskesmas;
        $model->asal_r = Yii::$app->user->identity->nama_rs_puskesmas;

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Rujukan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Rujukan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Rujukan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Rujukan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Rujukan::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionTes()
    {
        date_default_timezone_set("Asia/Jakarta");
        echo date('Y-m-d');
    }




    // <================================Akun RS==============================================================>
    public function actionUnreadCounter()
    {
        return Rujukan::find()->where(['status' => 'Menunggu'])->count();
    }
    public function actionMonitoring()
    {
        $searchModel = new RujukanSearch();
        $dataProvider = $searchModel->searchMonitoring(Yii::$app->request->queryParams);
        $dataProvider->pagination->pageSize=15;

        // print_r($dataProvider->getPagination());exit;

        return $this->render('rs-monitoring', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    //untuk render partial socket
    public function actionMonitoringPartial()
    {
        $searchModel = new RujukanSearch();
        $dataProvider = $searchModel->searchMonitoring(Yii::$app->request->queryParams);

        $dataProvider->pagination->pageSize=15;
        // $pagination = $dataProvider->getPagination();
        // $pagination->route = 'rujukan/monitoring';
        // $dataProvider->pagination->route = 'rujukan/monitoring';

        // print_r($dataProvider->getPagination());exit;

        return $this->renderPartial('rs-monitoring_partial', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionMonitoringNotif()
    {
        $unreadMessages = \app\Models\Rujukan::find()->where(['status' => 'Menunggu'])->count();

        return $this->renderPartial('rs-monitoring_notif', [
            'unreadMessages' => $unreadMessages,
        ]);
    }

    public function actionProses($id)
    {
        $model = $this->findModel($id);
        $model->scenario = 'prosesRujukan';

        if ($model->load(Yii::$app->request->post()))
        {
            // print_r($model); 
            // echo 'ads';
            // exit;

            if($model->save()){
                Yii::$app->getSession()->setFlash('success', 'Data Pasien berhasil diproses.');
                return $this->redirect(['rujukan/monitoring']);
                // return $this->redirect(['view', 'id' => $model->id]);
            }else{
                Yii::$app->getSession()->setFlash('danger', 'Data Pasien gagal diproses.');
                return $this->redirect(['rujukan/monitoring']);
            }
        }

        return $this->render('rs-proses', [
            'model' => $model,
        ]);
    }


}
