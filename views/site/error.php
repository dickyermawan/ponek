<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="site-error">

    <!-- <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-danger">
        <?= nl2br(Html::encode($message)) ?>
    </div>
 -->
    <i class="fa fa-ambulance" style="font-size: 10.5em;"></i>
    <i class="fa fa-hotel" style="font-size: 5.5em;"></i>
    <i class="fa fa-dot-circle-o" style="font-size: 2.5em;"></i>
    <i class="fa fa-dot-circle-o" style="font-size: 2.5em;"></i>
    <i class="fa fa-dot-circle-o" style="font-size: 2.5em;"></i>
    <i class="fa fa-dot-circle-o" style="font-size: 2.5em;"></i>
    <i class="fa fa-dot-circle-o" style="font-size: 2.5em;"></i>
    <i class="fa fa-dot-circle-o" style="font-size: 2.5em;"></i>
    <i class="fa fa-dot-circle-o" style="font-size: 2.5em;"></i>
    <i class="fa fa-dot-circle-o" style="font-size: 2.5em;"></i>
    <i class="fa fa-dot-circle-o" style="font-size: 2.5em;"></i>

    <p>
        Tolong bijak dalam menggunakan Internet ya.
    </p>
    <p>
        Namun, jika anda tersesat/ menganggap ini adalah kesalahan, anda dapat menghubungi Admin. Terimakasih <i class="fa fa-smile-o"></i>.
    </p>

</div>
