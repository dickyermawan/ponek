<?php

/*
* @Author 	: Dicky Ermawan S., S.T., MTA
* @Email 	: wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date 	: 2018-05-13 22:13:00
* @Last Modified by	 : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-05-27 05:45:42
*/

use yii\helpers\Url;

// $unreadMessages = $unreadMessages ? '<a class="unread-monitoring" href="'.Url::base().'/rujukan/monitoring">Monitoring <span class="badge blink_text" style="margin-left: 8px;">'.$unreadMessages.'</span></a>' : '<a class="unread-monitoring" href="'.Url::base().'/rujukan/monitoring">Monitoring</a>';

if($unreadMessages){
    //udah berhasil bunyi, belum tes otomatis dgn penambahan
	$unreadMessages = '<a class="unread-monitoring" href="'.Url::base().'/rujukan/monitoring">Monitoring <span class="badge blink_text" style="margin-left: 8px;">'.$unreadMessages.'</span></a>';
    if(Yii::$app->controller->id.'/'.Yii::$app->controller->action->id != 'rujukan/proses'){
    	$unreadMessages .= '<audio autoplay loop>
            <source src="'.Url::base().'/notif/ambulan1.ogg">
          </audio>';
    }
}else{
	$unreadMessages = '<a class="unread-monitoring" href="'.Url::base().'/rujukan/monitoring">Monitoring</a>';
}

echo $unreadMessages;