<?php

/*
* @Author 	: Dicky Ermawan S., S.T., MTA
* @Email 	: wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date 	: 2018-05-11 23:46:02
* @Last Modified by	 : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-05-12 11:27:32
*/

if($id=='Ibu'){
    $hasilUpdateJk = '<div id="rujukan-jenis-group" class="form-group field-fg-jk required">
                    <input type="hidden" id="rujukan-jk" name="Rujukan[jk]" value="Perempuan"></div>';
}
elseif($id=='Bayi'){
    $hasilUpdateJk = '<div id="rujukan-jenis-group" class="form-group field-fg-jk required">
	<label class="control-label col-sm-3">Jenis Kelamin</label>
	<div class="col-sm-9">
		<input type="hidden" name="Rujukan[jk]" value="">
		<div id="fg-jk" class="btn-group-md btn-group" data-toggle="buttons" aria-required="true">
			<label class="btn btn-default">
				<input type="radio" name="Rujukan[jk]" value="Laki-laki" data-index="0"> Laki-laki
			</label>
			<label class="btn btn-default">
				<input type="radio" name="Rujukan[jk]" value="Perempuan" data-index="1"> Perempuan
			</label>
		</div>
		<div class="help-block"></div>
	</div>
</div>';
}

echo $hasilUpdateJk;