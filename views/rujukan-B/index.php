<?php

/*
* @Author   : Dicky Ermawan S., S.T., MTA
* @Email    : wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date     : 2018-05-05 08:44:52
* @Last Modified by  : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-05-07 09:04:09
*/

use yii\helpers\Html;
use yii\grid\GridView;
use app\components\Penolong;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\RujukanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Riwayat Rujukan';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rujukan-index">

    <p>
        <?= Html::a('Tambah Rujukan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <div class="table-responsive">
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowOptions' => function($model) {
            if ($model->status==='Menunggu') return ['class' => 'danger'];
        },
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',
            [
                'attribute' => 'tgl_masuk',
                'headerOptions' => ['style' => 'width:14%'],
                'value' => function($model){
                    return Yii::$app->formatter->asDate($model->tgl_masuk);
                },
                'filterInputOptions' => [
                    'class'       => 'form-control',
                    'placeholder' => 'Contoh: 2018-12-23'
                 ]
            ],

            'ibu_nama',
            // 'ibu_tplahir',
            // 'ibu_tglahir',
            // 'ibu_umur',
            //'ibu_alamat:ntext',
            //'ibu_carabayar',
            //'bayi_jk',
            //'bayi_nama',
            //'bayi_tplahir',
            //'bayi_tglahir',
            //'bayi_umur',
            //'bayi_alamat:ntext',
            //'bayi_carabayar',

            [
                'attribute' => 'asal_rujukan_text',
                'header' => '<font color="#2fa4e">Asal Rujukan</font>',
                'value' => function ($model) {
                    return $model->asal_rujukan_text;
                },
                'format' => 'html',
            ],
            [
                'attribute' => 'tujuan_rujukan_text',
                'header' => '<font color="#2fa4e">Tujuan Rujukan</font>',
                'value' => function ($model) {
                    return $model->tujuan_rujukan_text;
                },
                'format' => 'html',
            ],


            // 'asal_rujukan:ntext',
            
            // 'tujuan_rujukan',

            //'tindakan_ygdiberikan:ntext',
            'alasan_rujukan:ntext',
            //'diagnosa:ntext',
            //'kesadaran',
            //'tekanan_darah',
            //'nadi',
            //'suhu',
            //'pernapasan',
            //'berat_badan',
            //'tinggi_badan',
            //'lila',
            //'nyeri',
            //'keterangan_lain:ntext',
            'info_balik:ntext',
            // 'status',
            [
                'headerOptions' => ['class' => 'text-center'],
                'contentOptions' => ['class' => 'text-center'],
                'attribute' => 'status',
                'value' => function ($model) {
                    return Penolong::label($model->status);
                },
                'format' => 'html',
            ],


            // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
    </div>
    
</div>
