<?php

/*
* @Author   : Dicky Ermawan S., S.T., MTA
* @Email    : wanasaja@gmail.com
* @Dashboard: http://dickyermawan.dev.php.or.id/
* @Date     : 2018-05-01 22:06:08
* @Last Modified by  : Dicky Ermawan S., S.T., MTA
* @Last Modified time: 2018-05-29 16:34:39
*/

namespace app\models;

use Yii;

/**
 * This is the model class for table "pengguna".
 *
 * @property int $id
 * @property string $hak_akses [ admin,rs,unit ]
 * @property string $username
 * @property string $password
 * @property string $nama_rs_puskesmas
 * @property string $kontak
 * @property string $email
 * @property string $alamat
 */
class Pengguna extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pengguna';
    }

    // public function beforeSave($insert) {
    //     if(isset($this->password)) 
    //         $this->password = Security::generatePasswordHash($this->password);
    //     return parent::beforeSave($insert);
    // }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'password'], 'required', 'message' => '{attribute} tidak boleh kosong.'],
            [['hak_akses', 'nama_rs_puskesmas', 'kontak', 'alamat'], 'required', 'message' => '{attribute} tidak boleh kosong.'],
            [['email'], 'safe'],

            [['username'], 'unique', 'on'=>'update', 'when' => function($model){
                    return static::getOldUsername(14) !== $model->username;
                }
            ],
            

            ['username', 'unique', 'on' => 'tambah'],

            // [['username'], 'unique','on' => 'tambah'],

            [['alamat'], 'string'],
            [['hak_akses'], 'string', 'max' => 15],
            [['username', 'password'], 'string', 'max' => 250],
            [['nama_rs_puskesmas'], 'string', 'max' => 250],
            [['kontak'], 'string', 'max' => 15],
            [['email'], 'string', 'max' => 100],
        ];
    }

    public static function getOldUsername($id)
    {
        return static::findIdentity($id)->username;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'hak_akses' => 'Hak Akses',
            'username' => 'Username',
            'password' => 'Password',
            'nama_rs_puskesmas' => 'Nama Rumah Sakit / Puskesmas',
            'kontak' => 'Kontak',
            'email' => 'Email',
            'alamat' => 'Alamat',
        ];
    }

}
